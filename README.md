# ExtApi - Example integration of Symfony/API-Platform with ExtJS

I've been developing separate ExtJS frontend webapp and Symfony backend APIs
for a while now and wedging them together behind Nginx when building containers
to run them. I've been thinking about ways to simplify things for a while and
decided to spend some time during the COVID-19 lockdown trying to make it 
work. This is the result of that effort.

TL;DR, it works but there are a couple [annoyances](#todo) I'm looking for
advice on.

## Step 1: Both Projects in One Folder

Start with a minimal Sencha webapp:

* `cd ~/projects`
* `ext-gen app -t moderndesktopminimal -m theme-material -n Eg`
* `cd eg`
* Started [this file...](README.md)

Step back and create a minimal Symfony app.

* `cd ~/projects`
* `composer create-project symfony/skeleton api`

Go back to the webapp folder and overlay the Symfony app.

* `cd eg`
* `cp .gitignore .gitignore.sencha`
* `(cd ../api && tar cf - .) | tar xf -`
* merge [.gitignore](.gitignore)

The [Makefile](Makefile) provides some automation. At this point:

* `make watch` builds the development webapp into `./build/...` (more on that
  later) then watches the code rebuilding when it sees changes.
* `make run` runs the PHP webserver exposing the development Symfony app.
* `make build` generates the production versions of both.

The webapp and website are not combined yet.

## Step 2: Cleanup

Before moving to integrate the two projects further, we're doing a little
cleanup in the ExtJS project.

* `main.js` is generated, should not be committed.
    * `rm main.js` and add it to .gitignore
* We will only ever have a single "build" so lets get rid of it. Lets also get
  rid of separate environment directories for generated output.
    * `mv app/desktop/src app/src`
    * `rm dir app/desktop`
    * edit `app.json`, edit `package.json` and `index.html`
        * remove ${build.id}, ${build.environment} and ${app.name} path elements.
* Move some pieces out of the top-level project folder down into [app/](app/).
    * `mv app.js app/app.js` and edit `app.json`
    * `mkdir app/sass`
    * `mv packages app/packages` and edit `workspace.json`
* Trimmed package.json primarily to prevent accidental publish.
* Add the Symfony API and profiler bits for later.
    * `composer require api` for API-Platform
    * `composer require --dev profiler` for developer support

Things still build but are still separate.

## Step 3: Expose the Webapp via Symfony

The intention here is for the `/app` URI to expose the ExtJS webapp while other
URIs (i.e. /, /api, etc) are handled by Symfony. We're going to have the webapp
build into `public/app` so it will simply be served up as static content by the
webserver. Nothing special in Symfony will be needed to make this work.

* set build.dir in workspace.json to "${workspace.dir}/public/app"
* set output.base in app.json to "${workspace.build.dir}"
* `mv index.html index.js app`
* set entryFile in webpack.config.js to './app/index.js'
* set outputFolder in webpack.config.js to './public/app'
* adjust the replace(...) command in webpack.config.js to set paths to ['app/index.html']
* change HtmlWebpackPlugin's template to "app/index.html" in webpack.config.js
* change bootstrap.base in app.json to "${app.dir}/public/app" and remove
  the generatedFiles/ prefix from the others under bootstrap.
* remove generatedFiles/ prefix from Ext.manifest in app/index.html
* set basehref in webpack.config.js to '/app/'

Various fiddiling in the configs and Makefile later... It appears to work.

* `make run` exposes the API and webapp via http://127.0.0.1:8000/
* use `make watch` to build and watch the develpment webapp
* use `make build` to build the production content

## TODO

As I mentioned earlier, there are a few annoying things I'd like to address:

* I'd like to get rid of the `<base href="/app/">` in
  [`app/index.html`](app/index.html) and have all of the referenced files
  simple be linked via relative URIs. Not sure where to start but this is a
  lower-priority concern.

* The links to the CSS and other `resource` files are relative to the root of
  the website (`/resources/*`) instead of `/app/resources/*`. The later is
  where they physically are but I'm not sure where to adjust how these links are
  constructed. A softlink was needed to make this work in development. Curiously,
  the link isn't needed when I build the production version of the webapp.

* When running in development mode, the webapp loads sources from `/app/**.js`
  and `/node_modules/**.js`. I have softlinks to make this work.

At this point, I'm looking for some guidance from Sencha on how to better 
address these last few issues. However, it looks like it should work and be
maintainable without much hassle. I'm looking forward to trying it out on my
next project.A

-- Paul Dugas <paul@dugas.cc>
