CONSOLE = ./bin/console
SENCHA = ./node_modules/@sencha/cmd/bin/sencha

ifeq (,$(wildcard $(CONSOLE)))
  $(error error: $(CONSOLE) missing)
endif
ifeq (,$(shell which composer))
  $(error error: composer not in path)
endif
ifeq (,$(shell which php))
  $(error error: php not in path)
endif
ifeq (,$(shell which npm))
  $(error error: npm not in path)
endif

build: install
	$(RM) -r public/app
	$(RM) public/resources
	$(RM) public/node_modules
	$(SENCHA) app build
	$(CONSOLE) -e prod cache:warmup 

install: vendor node_modules

vendor: composer.json
	composer install --no-progress

node_modules: package.json
	npm install --no-fund --no-audit

watch: install
	$(RM) -r public/app
	mkdir -p public/app
	(cd public/app && ln -s ../../app/app.js)
	(cd public/app && ln -s ../../app/src)
	(cd public && ln -f -s ../node_modules)
	(cd public && ln -s app/resources)
	cp app/index.html public/app
	$(SENCHA) app watch

run: install
	php -S 127.0.0.1:8000 -t public

db: vendor
	$(CONSOLE) doctrine:database:drop --force
	$(CONSOLE) doctrine:database:create
	$(CONSOLE) doctrine:schema:create -n

clean: 
	@$(RM) -r public/bundles public/app vendor node_modules build main.js temp.txt
	@$(RM) -r var/log var/cache
	@if $(CONSOLE) >/dev/null 2>&1; then \
	  $(CONSOLE) -e prod doctrine:cache:clear-metadata --flush -q; \
	  $(CONSOLE) -e prod doctrine:cache:clear-query --flush -q; \
	  $(CONSOLE) -e prod doctrine:cache:clear-result --flush -q; \
	fi 
	@$(RM) -r .env.local .env.*.local .env.local.php
	@$(RM) *.log

.PHONY: build run db clean install watch
